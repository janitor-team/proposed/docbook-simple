#!/usr/bin/make -f

include /usr/share/dpkg/default.mk

%:
	dh $@

override_dh_install: debian/docbook-simple.install \
             debian/docbook-simple.links \
             debian/docbook-simple.sgmlcatalogs \
             debian/docbook-simple.xmlcatalogs
	dh_install

override_dh_clean:
	dh_clean debian/docbook-simple.install
	dh_clean debian/docbook-simple.links
	dh_clean debian/docbook-simple.sgmlcatalogs
	dh_clean debian/docbook-simple.xmlcatalogs

override_dh_installcatalogs:
	dh_installxmlcatalogs
	dh_installcatalogs

debian/docbook-simple.install:
	set -ex ; \
	touch $@ ; \
	for dbs in docbook-simple-* ; do \
		dbsver=`echo $${dbs} | sed -e 's/docbook-simple-//g'` ; \
		echo "$${dbs}/catalog.xml	usr/share/xml/docbook/custom/simple/$${dbsver}" >> $@ ; \
		echo "$${dbs}/*.mod	usr/share/xml/docbook/custom/simple/$${dbsver}" >> $@ ; \
		echo "$${dbs}/sdocbook.dtd	usr/share/xml/docbook/custom/simple/$${dbsver}" >> $@ ; \
		echo "$${dbs}/sdocbookref.dtd	usr/share/xml/docbook/custom/simple/$${dbsver}" >> $@ ; \
		echo "$${dbs}/*.css	usr/share/xml/docbook/custom/simple/$${dbsver}/css" >> $@ ; \
		echo "$${dbs}/sdocbook*-custom.dtd	etc/sgml/docbook-simple/$${dbsver}" >> $@ ; \
		echo >> $@ ; \
	done

debian/docbook-simple.links:
	set -ex ; \
	touch $@ ; \
	cat $@.in >> $@ ; \
	for dbs in docbook-simple-*/sdocbook-custom.dtd docbook-simple-*/sdocbookref-custom.dtd ; do \
		dbsdtd=`echo $${dbs} | sed -e 's/docbook-simple-//g'` ; \
		echo -n "etc/sgml/docbook-simple/$${dbsdtd} " >> $@ && \
		    echo "usr/share/xml/docbook/custom/simple/$${dbsdtd}" >> $@ ; \
	done

debian/docbook-simple.sgmlcatalogs:
	set -ex ; \
	touch $@ ; \
	for dbs in docbook-simple-* ; do \
		dbsver=`echo $${dbs} | sed -e 's/docbook-simple-//g'` ; \
		echo "$${dbs}/catalog	/usr/share/xml/docbook/custom/simple/$${dbsver}/catalog" >> $@ ; \
	done

debian/docbook-simple.xmlcatalogs:
	set -ex ; \
	touch $@ ; \
	cat $@.in >> $@ ; \
	for dbscat in docbook-simple-*/catalog.xml ; do \
		dbsver=`echo $${dbscat} | sed -e 's/docbook-simple-//g' -e 's/\/catalog.xml//g'` ; \
		echo "local;$${dbscat};/usr/share/xml/docbook/custom/simple/$${dbsver}/catalog.xml" >> $@ ; \
		sysids=`egrep -e "<system systemId=\"[^\"]+\"" $${dbscat} | \
		    sed -e 's/<system systemId=\"\(.*\)\"/\1;/g'` IFS=';' ; \
		echo $${sysids} | while read id ; do \
			echo "package;system;$${id};/usr/share/xml/docbook/custom/simple/$${dbsver}/catalog.xml" >> $@ ; \
		done ; \
		pubids=`egrep -e "<public publicId=\"[^\"]+\"" $${dbscat} | \
		    sed -e 's/<public publicId=\"\(.*\)\"/\1;/g'` IFS=';' ; \
		echo $${pubids} | while read id ; do \
			echo "package;public;$${id};/usr/share/xml/docbook/custom/simple/$${dbsver}/catalog.xml" >> $@ ; \
		done ; \
		echo >> $@ ; \
	done ; \
	sed -i -e 's/\ ;\/usr/;\/usr/g' $@

# This following code is used to create the .orig.tar.gz tarball.
# Use it to prepare it for a new release.

version=$(shell dpkg-parsechangelog | sed -n -e 's/^Version: \(.*\)-[^-]*/\1/p')

.PHONY: get-orig-source
get-orig-source:
	set -ex ; \
	TMPDIR=`mktemp -d docbook-simple-$(version).orig` ; \
	for dbs in 1.0 1.1 ; do \
		mkdir -p "$$TMPDIR"/docbook-simple-$${dbs} ; \
		dbszip=docbook-simple-$${dbs}.zip ; \
		wget -P "$$TMPDIR" http://www.oasis-open.org/docbook/xml/simple/$${dbs}/"$$dbszip" ; \
		unzip -d "$$TMPDIR"/docbook-simple-$${dbs} "$$TMPDIR"/"$$dbszip" ; \
		rm -f "$$TMPDIR"/"$$dbszip" ; \
	done; \
	find "$$TMPDIR" -type f ! -perm 644 -exec chmod 644 "{}" ";" ; \
	GZIP=-9 tar -czf $(CURDIR)/docbook-simple_${version}.orig.tar.gz "$$TMPDIR" ; \
	rm -rf "$$TMPDIR"
